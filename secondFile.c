#include <stdio.h>
#include <vxWorks.h>

void secondFile()
{
    int secondInt=1;
    printf("\nThis is the second file\n");

    while (secondInt < 10)
    {
        printf("secondInt is %d\n", secondInt);
        secondInt++;
    }
}
