#include <stdio.h>
#include <vxWorks.h>

void studioImport()
{
	int studioInt = 1;
	printf("\n Currently studioInt is %d\n", studioInt);
	while (studioInt < 100)
	{
		studioInt++;
		printf("Increased studioInt to %d\n", studioInt);
	}
}